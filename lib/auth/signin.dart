import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_chat_poc/home.dart';
import 'package:quickblox_sdk/auth/module.dart';
import 'package:quickblox_sdk/models/qb_user.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';
import 'package:provider/provider.dart';

import '../models/user.dart';

class SignIn extends StatefulWidget {
  SignIn({Key key}) : super(key: key);

  @override
  _SignIn createState() => _SignIn();
}

class _SignIn extends State<SignIn> {
  void connect(int userId, String userPassword) async {
    try {
      await QB.chat.connect(userId, userPassword);
    } on PlatformException catch (e) {
      debugPrint(e.message);
    }
  }

  void loginUser(BuildContext context, String login, String password) async {
    try {
      QBLoginResult result = await QB.auth.login(login, password);
      QBUser qbUser = result.qbUser;

      // Add user to state
      context.read<User>().logUser(qbUser.login, qbUser.id);

      // Connect to chat
      connect(qbUser.id, password);

      // Navigate to home
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => MyHomePage()));
    } on PlatformException catch (e) {
      debugPrint(e.message);
    }
  }

  @override
  Widget build(BuildContext context) {
    final passwordController = TextEditingController();
    final loginController = TextEditingController();

    final loginField = TextField(
        obscureText: false,
        decoration: InputDecoration(
          hintText: "Login",
        ),
        controller: loginController);
    final passwordField = TextField(
        obscureText: true,
        decoration: InputDecoration(
          hintText: "Password",
        ),
        controller: passwordController);
    final loginButon = Material(
      elevation: 5.0,
      color: Colors.blue[200],
      child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () =>
              loginUser(context, loginController.text, passwordController.text),
          child: Text(
            "Sign in",
            textAlign: TextAlign.center,
          )),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('Sign in'),
      ),
      body: Center(
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 45.0),
                loginField,
                SizedBox(height: 25.0),
                passwordField,
                SizedBox(
                  height: 35.0,
                ),
                loginButon,
                SizedBox(
                  height: 15.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
