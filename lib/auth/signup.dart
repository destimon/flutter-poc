import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_chat_poc/home.dart';
import 'package:quickblox_sdk/models/qb_user.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';

class SignUp extends StatefulWidget {
  SignUp({Key key}) : super(key: key);

  @override
  _SignUp createState() => _SignUp();
}

class _SignUp extends State<SignUp> {
  void createUser(login, password) async {
    try {
      QBUser user = await QB.users.createUser(login, password);

      Navigator.push(
          context, MaterialPageRoute(builder: (context) => MyHomePage()));
    } on PlatformException catch (e) {
      // Some error occured, look at the exception message for more details
    }
  }

  @override
  Widget build(BuildContext context) {
    final passwordController = TextEditingController();
    final loginController = TextEditingController();

    final loginField = TextField(
        obscureText: false,
        decoration: InputDecoration(
          hintText: "Login",
        ),
        controller: loginController);
    final passwordField = TextField(
        obscureText: true,
        decoration: InputDecoration(
          hintText: "Password",
        ),
        controller: passwordController);
    final loginButon = Material(
      elevation: 5.0,
      color: Colors.blue[200],
      child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () =>
              createUser(loginController.text, passwordController.text),
          child: Text(
            "Sign up",
            textAlign: TextAlign.center,
          )),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('Sign up'),
      ),
      body: Center(
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 45.0),
                loginField,
                SizedBox(height: 25.0),
                passwordField,
                SizedBox(
                  height: 35.0,
                ),
                loginButon,
                SizedBox(
                  height: 15.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
