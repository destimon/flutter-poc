import 'package:flutter/material.dart';
import 'package:flutter_chat_poc/list_chats.dart';
import 'auth/signup.dart';
import 'package:flutter_chat_poc/models/user.dart';
import 'package:provider/provider.dart';
import 'create.dart';
import 'auth/signin.dart';

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    signButton(label) {
      return Material(
        color: Colors.blue[200],
        child: MaterialButton(
            onPressed: () {
              if (label == 'Sign In') {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => SignIn()));
              } else if (label == 'Sign Up') {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => SignUp()));
              }
            },
            child: Text(
              label,
              textAlign: TextAlign.center,
            )),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('PoC Flutter Chat'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(36.0),
          child: Column(
            children: <Widget>[
              if (context.watch<User>().isLogged == false) ...[
                Text("Please, choose operation:"),
                SizedBox(height: 35.0),
                signButton('Sign In'),
                SizedBox(height: 35.0),
                signButton('Sign Up'),
              ] else ...[
                Text('Hello, ' + context.watch<User>().login),
                Text('Your ID: ' + context.watch<User>().id.toString()),
                Text("Please, choose operation:"),
                MaterialButton(
                  onPressed: () => Navigator.push(context,
                      MaterialPageRoute(builder: (context) => CreateChat())),
                  child: Text("Create"),
                  elevation: 5.0,
                  minWidth: MediaQuery.of(context).size.width,
                  color: Colors.blue[200],
                ),
                MaterialButton(
                  onPressed: () => Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ListChats())),
                  child: Text("List"),
                  elevation: 5.0,
                  minWidth: MediaQuery.of(context).size.width,
                  color: Colors.blue[200],
                ),
              ]
            ],
          ),
        ),
      ),
    );
  }
}
