import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class User with ChangeNotifier, DiagnosticableTreeMixin {
  bool _isLogged = false;
  String _login = '';
  int _id;

  String get login => _login;
  bool get isLogged => _isLogged;
  int get id => _id;

  void logUser(newLogin, id) {
    _login = newLogin;
    _id = id;
    _isLogged = true;
    notifyListeners();
  }

  void logoutUser() {
    _isLogged = false;
    _login = '';
    _id = null;
  }
}
