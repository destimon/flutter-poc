import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quickblox_sdk/models/qb_user.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';

class Users with ChangeNotifier, DiagnosticableTreeMixin {
  List _users = [];

  List get users => _users;

  void fetchUsers() async {
    try {
      List<QBUser> userList = await QB.users.getUsers();

      _users = userList;
    } on PlatformException catch (e) {
      debugPrint(e.message);
    }
    notifyListeners();
  }
}
