import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Messages with ChangeNotifier, DiagnosticableTreeMixin {
  List _messages = [];

  List get messages => _messages;

  void pushMessage(newMessage) async {
    _messages = [..._messages, newMessage];
    notifyListeners();
  }
}
