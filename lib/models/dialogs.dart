import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';

class Dialogs with ChangeNotifier, DiagnosticableTreeMixin {
  List<QBDialog> _dialogs = [];

  List<QBDialog> get dialogs => _dialogs;

  void fetchDialogs() async {
    try {
      List<QBDialog> dialogs = await QB.chat.getDialogs();
      _dialogs = dialogs;
    } on PlatformException catch (e) {
      debugPrint(e.message);
    }

    notifyListeners();
  }
}
