import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_chat_poc/models/messages.dart';
import 'package:flutter_chat_poc/models/dialogs.dart';
import 'package:flutter_chat_poc/models/users.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';
import 'package:provider/provider.dart';
import 'config/quickblox_init.dart';
import 'home.dart';
import 'models/user.dart';

void main() {
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(create: (_) => User()),
    ListenableProvider(create: (context) => Dialogs()),
    ListenableProvider(create: (context) => Messages()),
    ListenableProvider(create: (context) => Users())
  ], child: MyApp()));
}

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  _MyApp createState() => _MyApp();
}

class _MyApp extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) => init(context));
  }

  @override
  void dispose() async {
    QB.auth.logout();
    context.read<User>().logoutUser();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'Flutter chat', home: MyHomePage());
  }
}
