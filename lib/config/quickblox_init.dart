import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_chat_poc/config/envs.dart';
import 'package:flutter_chat_poc/models/dialogs.dart';
import 'package:flutter_chat_poc/models/users.dart';
import 'package:quickblox_sdk/chat/constants.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';
import 'package:provider/provider.dart';

import '../models/messages.dart';

void enableAutoReconnect() async {
  try {
    await QB.settings.enableAutoReconnect(true);
  } on PlatformException catch (e) {
    debugPrint(e.message);
  }
}

void listenMessages(BuildContext context) async {
  try {
    await QB.chat.subscribeChatEvent(QBChatEvents.RECEIVED_NEW_MESSAGE, (data) {
      Map<String, Object> map = new Map<String, dynamic>.from(data);
      Map<String, Object> payload =
          new Map<String, dynamic>.from(map["payload"]);

      context.read<Messages>().pushMessage(payload);
    });
  } on PlatformException catch (e) {
    // Some error occured, look at the exception message for more details
  }
}

void init(BuildContext context) async {
  try {
    await QB.settings.init(APP_ID, AUTH_KEY, AUTH_SECRET, ACCOUNT_KEY);

    enableAutoReconnect();
    context.read<Dialogs>().fetchDialogs();
    context.read<Users>().fetchUsers();
    listenMessages(context);
  } on PlatformException catch (e) {
    debugPrint(e.message);
  }
}
