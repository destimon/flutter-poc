// Flutter
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_chat_poc/models/messages.dart';
import 'package:flutter_chat_poc/models/users.dart';
import 'package:provider/provider.dart';
import 'package:quickblox_sdk/models/qb_user.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';

class ChatDialog extends StatefulWidget {
  final dialogName;
  final dialogId;

  ChatDialog({Key key, this.dialogName, this.dialogId}) : super(key: key);

  @override
  _ChatDialog createState() => _ChatDialog();
}

class _ChatDialog extends State<ChatDialog> {
  get login => null;

  sendNewMessage(String message) async {
    try {
      await QB.chat
          .sendMessage(widget.dialogId, body: message, saveToHistory: true);
    } on PlatformException catch (e) {
      debugPrint(e.message);
    }
  }

  @override
  Widget build(BuildContext context) {
    final messages = context
        .watch<Messages>()
        .messages
        ?.where((m) => m["dialogId"] == widget?.dialogId)
        ?.toList();
    final messageController = TextEditingController();

    List<QBUser> users = context.watch<Users>().users;

    final sendButton = Material(
      elevation: 5.0,
      color: Colors.blue[200],
      child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () => sendNewMessage(messageController.text),
          child: Text(
            "Send",
            textAlign: TextAlign.center,
          )),
    );

    messageDisplay(message) {
      return Material(
          color: Colors.blue[200],
          child: Row(children: <Widget>[
            Text(
              users
                      ?.firstWhere((user) => message["senderId"] == user.id,
                          orElse: () => null)
                      ?.login ??
                  "UNKNOWN",
              textAlign: TextAlign.left,
            ),
            Text(
              ": ",
              textAlign: TextAlign.left,
            ),
            Text(
              message["body"],
              textAlign: TextAlign.left,
            )
          ]));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Dialog'),
      ),
      body: Padding(
          padding: const EdgeInsets.all(36.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Dialog ID:' + widget.dialogId, textAlign: TextAlign.left),
              SizedBox(
                height: 10.0,
              ),
              Text('Dialog Name: ' + widget.dialogName,
                  textAlign: TextAlign.left),
              SizedBox(
                height: 10.0,
              ),
              TextField(
                decoration: InputDecoration(hintText: 'Your message...'),
                controller: messageController,
              ),
              SizedBox(
                height: 20.0,
              ),
              sendButton,
              SizedBox(
                height: 35.0,
              ),
              Text('Messages:', textAlign: TextAlign.left),
              SizedBox(
                height: 20.0,
              ),
              Column(
                children: List.generate(messages.length,
                    (index) => messageDisplay(messages[index])),
              )
            ],
          )),
    );
  }
}
