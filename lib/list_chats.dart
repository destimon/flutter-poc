import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_chat_poc/models/dialogs.dart';
import 'package:flutter_chat_poc/models/users.dart';
import 'package:quickblox_sdk/models/qb_dialog.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';
import 'package:provider/provider.dart';

import 'chat_dialog.dart';

class ListChats extends StatefulWidget {
  @override
  _ListChats createState() => _ListChats();
}

class _ListChats extends State<ListChats> {
  listChats(chats) async {
    try {
      List<QBDialog> dialogs = await QB.chat.getDialogs();

      return dialogs;
    } on PlatformException catch (e) {
      debugPrint(e.message);
    }
  }

  @override
  void initState() {
    super.initState();
    context.read<Dialogs>().fetchDialogs();
    context.read<Users>().fetchUsers();
  }

  @override
  Widget build(BuildContext context) {
    final chats = context.watch<Dialogs>().dialogs;

    chatItem(id, label) {
      return Material(
        elevation: 5.0,
        color: Colors.blue[200],
        child: MaterialButton(
            minWidth: MediaQuery.of(context).size.width,
            onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ChatDialog(
                          dialogId: id,
                          dialogName: label,
                        ))),
            child: Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  label,
                  textAlign: TextAlign.center,
                ))),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('PoC Flutter Chat'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(36.0),
        child: Column(
          children: List.generate(
            chats.length,
            (index) => chatItem(chats[index].id, chats[index].name),
          ),
        ),
      ),
    );
  }
}
