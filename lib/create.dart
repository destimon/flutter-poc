import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';

class CreateChat extends StatelessWidget {
  void createDialog(
      String dialogName, int dialogType, List<int> occupantIds) async {
    try {
      await QB.chat
          .createDialog(occupantIds, dialogName, dialogType: dialogType);
    } on PlatformException catch (e) {
      debugPrint(e.message);
      // Some error occured, look at the exception message for more details
    }
  }

  List<int> getParsedIds(String ids) {
    return ids.split(',').map((id) => int.parse(id)).toList();
  }

  @override
  Widget build(BuildContext context) {
    final dialogNameController = TextEditingController();
    final typeController = TextEditingController();
    final occIdsController = TextEditingController();

    final createButton = Material(
      elevation: 5.0,
      color: Colors.blue[200],
      child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () => createDialog(
              dialogNameController.text,
              int.parse(typeController.text),
              getParsedIds(occIdsController.text)),
          child: Text(
            "Create chat",
            textAlign: TextAlign.center,
          )),
    );

    return Scaffold(
        appBar: AppBar(
          title: Text('PoC Flutter Chat'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(36.0),
          child: Column(
            children: <Widget>[
              SizedBox(height: 20.0),
              TextField(
                decoration: InputDecoration(hintText: 'Chat name'),
                controller: dialogNameController,
              ),
              SizedBox(height: 20.0),
              TextField(
                decoration: InputDecoration(
                    hintText: 'Occupant ids(separated by coma)'),
                controller: occIdsController,
              ),
              SizedBox(height: 20.0),
              TextField(
                decoration: InputDecoration(hintText: 'Chat type(1/2/3)'),
                controller: typeController,
              ),
              SizedBox(height: 20.0),
              Padding(
                padding: const EdgeInsets.all(25.0),
                child: Text(
                    "Type of a new dialog. Possible values are: type=1 (PUBLIC_GROUP), type=2 (GROUP), type=3 (PRIVATE)."),
              ),
              SizedBox(height: 25.0),
              createButton,
            ],
          ),
        ));
  }
}
